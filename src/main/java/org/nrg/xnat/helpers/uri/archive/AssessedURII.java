/*
 * web: org.nrg.xnat.helpers.uri.archive.AssessedURII
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.helpers.uri.archive;

import org.nrg.xdat.om.XnatImagesessiondata;

public interface AssessedURII {
	public XnatImagesessiondata getSession();
}
